# -*- coding: utf-8 -*-

from LineAPI.linepy import *
from LineAPI.linepy import LINE, OEPoll
from LineAPI.akad.ttypes import ChatRoomAnnouncementContents, OpType, MediaType, ContentType, ApplicationType, TalkException, ErrorCode
import multiprocessing
from multiprocessing import Process
from Naked.toolshed.shell import execute_js
import threading, traceback, newqr
#import QRLogin
from qr import QRLogin
from random import randint
from shutil import copyfile
from youtube_dl import YoutubeDL
import subprocess, youtube_dl, humanize, traceback
import subprocess as cmd
import platform
#import QRLogin

try:
    import urllib.request as urllib2
except ImportError:
    import urllib2
qrv2 = QRLogin()
result = qrv2.loginWithQrCode("ipad")
#=====================================================================
APP = "IOSIPAD\t10.1.1\tiPhone 8\t11.2.5"
me = LINE(result.accessToken,appName=APP)
print(me.authToken)
admin = ["u6d01cfeeca6803100ed414528a64a11d","u7e3584e11398b6bfa0caf670f5c48a45","ufd2aca4b8df82fd03b9c4135cb304e74"]
oepoll = OEPoll(me)

def toChar(text):
    normal = 'abcdefghijklmnopqrstuvwxyz'
    tochange = 'abcdefghijklmnopqrstuvwxyz'
    for i in range(len(normal)):
        text = text.lower().replace(normal[i], tochange[i])
    return text

helpKick = toChar('''คำสั่งบอทบิน (s/+)
- หาย↭ลบกลุ่ม
- oo [@, name]↭เตะ
- check↭เช็คบัค
**เฉพาะแอดมินดึงเข้ากลุ่มเตะทันที''')

def clientBot(op):
    global me
    try:
        if op.type == 13:
          if op.param1 in admin:
             if op.param2 :
                try:
                   me.acceptGroupInvitation(op.param1)
                   mem = [c.mid for c in me.getGroup(op.param1).members]
                   targets = []
                   for x in mem:
                      if x not in admin:
                        targets.append(x)
                   if targets:
                       imnoob = 'simple.js gid={} token={} app={}'.format(op.param1, me.authToken, "IOSIPAD\t10.1.1\tiPhone 8\t11.2.5")
                       for target in targets:
                          imnoob += ' uid={}'.format(target)
                       success = execute_js(imnoob)
                       if success:
                          me.sendMessage(op.param1, "เตะสำเร็จ %i คน" % len(targets))
                       else:
                          me.sendMessage(op.param1, 'ล้มเหลวเตะ %i คน' % len(targets))
                   else:
                      me.sendMessage(op.param1, 'ไม่พบเป้าหมาย')
                except:
                   me.acceptGroupInvitation(op.param1)
                   me.sendMessage(op.param1,'Limited')
 
        if op.type in [25,26]:
            msg = op.message
            to = msg.to
            makeText = str(msg.text)
            if makeText == None:return
            
            if makeText.lower() == 'mid':
               me.sendMessage(to, msg._from)
            
            if makeText.lower() in ['คำสั่งบอทบิน', 'help']:
                me.sendMessage(msg.to, str(helpKick))
            
            if makeText.lower() == 'หาย': 
                mem = [c.mid for c in me.getGroup(to).members]
                targets = []
                for x in mem:
                    if x not in admin:
                       targets.append(x)
                if targets:
                   imnoob = 'simple.js gid={} token={} app={}'.format(to, me.authToken, "IOSIPAD\t10.1.1\tiPhone 8\t11.2.5")
                   for target in targets:
                      imnoob += ' uid={}'.format(target)
                   success = execute_js(imnoob)
                   if success:
                      me.sendMessage(to, "เตะสำเร็จ %i คน" % len(targets))
                   else:
                      me.sendMessage(to, 'ล้มเหลวเตะ %i คน' % len(targets))
                else:
                    me.sendMessage(to, 'ไม่พบเป้าหมาย')
                    
            if makeText.lower().startswith("oo"):
               try:
                    sep = makeText.split(" ")
                    midn = makeText.replace(sep[0] + " ","")
                    G = me.getGroup(msg.to)
                    members = [G.mid for G in G.members]
                    targets = []
                    imnoob = 'simple.js gid={} token={} app={}'.format(to, me.authToken, "IOSIPAD\t10.1.1\tiPhone 8\t11.2.5")
                    for mids in members:
                       contact = me.getContact(mids)
                       testt = contact.displayName.lower()
                       if midn in testt:
                           targets.append(contact.mid)
                    if targets == []:
                       return me.sendMessage(to, toChar("not found name "+midn)) 
                    for target in targets:
                       imnoob += ' uid={}'.format(target)
                    success = execute_js(imnoob)
               except:pass
               


    except Exception as e:print(e)
                  
def run():
    while True:
        try:
            ops = oepoll.singleTrace(count=50)
            if ops != None:
                for op in ops:
                    threads = []
                    for i in range(1):
                        thread = threading.Thread(target=clientBot(op))
                        threads.append(thread)
                        thread.start()
                        oepoll.setRevision(op.revision)
            for thread in threads:
                thread.join()
        except: pass
if __name__ == "__main__":
    run()